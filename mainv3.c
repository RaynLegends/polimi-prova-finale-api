#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*#define DEBUG_ENTITIES
#define DEBUG
#define INFO*/
//#define COLLISIONS
//#define FLUSHPRINT

#define ENTITY_DB_SIZE 32768
#define RELATION_DB_SIZE 4
#define ENTITY_RELATION_SIZE 1
#define ENTITY_RELATION_RELATED_ENTITIES_SIZE 1

#define INPUT_LENGTH 103

#define MAX_ID_ENT_SIZE 31
#define MAX_ID_REL_SIZE 31

/* Structs */
typedef struct entity_s {
    int valid;
    int hash;
    char id_ent[MAX_ID_ENT_SIZE];
} entity_t;

typedef struct entity_relation_s {
    entity_t * entity;
    int valid_related_entities;
    int related_entities_length;
    entity_t ** related_entities; /* Array of related entities */
} entity_relation_t;

typedef struct relation_s {
    int valid;
    char id_rel[MAX_ID_REL_SIZE];
    int valid_entity_relations;
    int entity_relations_length;
    entity_relation_t * entity_relations;
} relation_t;
/* End of Structs */


/* Fields */
entity_t * entities;
int entities_length;

relation_t * relations;
int valid_relations;
int relations_length;

entity_t ** entitiesTable;

#ifdef COLLISIONS
int table_collisions;
int table_searches;
#endif
/* End of Fields */


/* Prototypes */
void addent(char * id_ent);
void delent(char * id_ent);
void addrel(char * id_dest, char * id_orig, char * id_rel);
void delrel(char * id_dest, char * id_orig, char * id_rel);
void report();


void entities_init();
void entities_grow();
int entity_create(char * id_ent, int hash);
int entity_hash(const char * id_ent);
entity_t * entity_search(char * id_ent);
entity_t * entity_search_with_hash(char * id_ent, int hash);
void entity_add(entity_t * entity);
int entity_is_equal(entity_t * entity1, entity_t * entity2);
int entity_is_equal_mixed(entity_t * entity1, char * id_ent2, int hash2);
int entity_is_equal_by_id(char * id_ent1, char * id_ent2);


void relations_init();
void relations_grow();
relation_t * relation_create(char * id_rel);
relation_t * relation_search(char * id_rel);
void entity_relations_grow(relation_t * relation);
entity_relation_t * entity_relation_add(relation_t * relation, entity_t * entity);
entity_relation_t * entity_relation_search(relation_t * relation, entity_t * entity);
entity_relation_t * entity_relation_search_by_id(relation_t * relation, char * id_ent);
void entity_relation_add_related(entity_relation_t * entity_relation, entity_t * related);


void relations_remove_entity(entity_t * entity);

#ifdef DEBUG
void debug_print_entities();
void debug_print_entitiesTable();
void debug_print_relations();
#endif
/* End of Prototypes */


int main(int argc, char * argv[]) {
    int i, scan, arg, argpos;
    char line[INPUT_LENGTH + 1];
    char id_ent1[MAX_ID_ENT_SIZE];
    char id_ent2[MAX_ID_ENT_SIZE];
    char id_rel[MAX_ID_REL_SIZE];

    scan = 1;
#ifdef COLLISIONS
    table_collisions = 0;
    table_searches = 0;
#endif

#ifdef FLUSHPRINT
    setbuf(stdout, NULL);
#endif

    entities_init();
    relations_init();

    while (scan) {
        argpos = 0;

        fgets(line, INPUT_LENGTH + 1, stdin);

        if (line[0] == 'e' && line[1] == 'n' && line[2] == 'd') {
            scan = 0;
        } else if (line[0] == 'r') {
            report();
        } else {
            i = 7;

            while (1) {
                if (line[i] != '"') {
                    id_ent1[argpos++] = line[i];
                } else if (argpos > 0) {
                    id_ent1[argpos] = '\0';
                    break;
                }

                i++;
            }

            if (line[3] == 'e') {
                if (line[0] == 'a') {
                    addent(id_ent1);
                } else {
                    delent(id_ent1);
                }
            } else {
                i += 2;
                arg = 0;
                argpos = 0;

                while (1) {
                    if (line[i] == ' ') {
                        id_ent2[argpos] = '\0';
                        arg++;
                        argpos = 0;
                    } else if (line[i] != '"') {
                        if (arg == 0) {
                            id_ent2[argpos++] = line[i];
                        } else {
                            id_rel[argpos++] = line[i];
                        }
                    } else if (arg >= 1 && argpos > 0) {
                        id_rel[argpos] = '\0';
                        break;
                    }

                    i++;
                }

                if (line[0] == 'a') {
                    addrel(id_ent1, id_ent2, id_rel);
                } else {
                    delrel(id_ent1, id_ent2, id_rel);
                }
            }
        }

#ifdef DEBUG
        printf("\n\n\n");
#endif
    }

#ifdef COLLISIONS
    printf("#%d collisions out of #%d searches (Rate: %0.2f)",
            table_collisions,
            table_searches,
            100 * (float) table_collisions / (float) table_searches
            );
#endif
    return 0;
}

void addent(char * id_ent) {
    int hash = entity_hash(id_ent);
    entity_t * entity = entity_search_with_hash(id_ent, hash);

    if (entity != NULL) {
#ifdef INFO
        printf("Entity found: %s - %d\n", entity->id_ent, entity->valid);
#endif
        return;
    }

    entity_create(id_ent, hash);

#ifdef INFO
    printf("Added entity %s\n", id_ent);
#endif
}

void delent(char * id_ent) {
    entity_t * entity = entity_search(id_ent);

    if (entity == NULL) {
#ifdef INFO
        printf("Entity not found: %s\n", id_ent);
#endif
        return;
    }

    relations_remove_entity(entity);

    entity->valid = 0;
    entity->id_ent[0] = '\0';

#ifdef INFO
    printf("Removed entity %s\n", id_ent);
#endif
}

/* Swap orig and dest so that report data is easier to extract */
void addrel(char * id_dest, char * id_orig, char * id_rel) {
#ifdef INFO
    printf("Executing addrel on %s %s %s\n", id_orig, id_dest, id_rel);
#endif
    entity_t * entity_orig;
    entity_t * entity_dest;
    relation_t * relation;
    entity_relation_t * entity_relation;

    entity_orig = entity_search(id_orig);
    if (entity_orig == NULL) {
#ifdef INFO
        printf("ENTITY ORIG NOT FOUND: %s\n", id_orig);
#endif
        return;
    }

    entity_dest = entity_search(id_dest);
    if (entity_dest == NULL) {
#ifdef INFO
        printf("ENTITY DEST NOT FOUND: %s\n", id_dest);
#endif
        return;
    }

    relation = relation_search(id_rel);
    if (relation == NULL) {
        relation = relation_create(id_rel);
        entity_relation = NULL;
    } else {
        entity_relation = entity_relation_search(relation, entity_orig);
    }

    if (entity_relation == NULL) {
        entity_relation = entity_relation_add(relation, entity_orig);
    }

    entity_relation_add_related(entity_relation, entity_dest);

#ifdef INFO
    printf("Executed addrel on %s %s %s\n", id_orig, id_dest, id_rel);
#endif

#ifdef DEBUG
    debug_print_relations();
#endif
}

/* Swap orig and dest so that report data is easier to extract */
void delrel(char * id_dest, char * id_orig, char * id_rel) {
#ifdef INFO
    printf("Executing delrel on %s %s %s\n", id_orig, id_dest, id_rel);
#endif
    int bot, mid, top, compare;
    entity_t * entity_orig;
    entity_t * entity_dest;
    relation_t * relation;
    entity_relation_t * entity_relation;
    entity_relation_t entity_relation_temp;
    int i, entity_relation_id;

    entity_orig = entity_search(id_orig);
    if (entity_orig == NULL) {
#ifdef INFO
        printf("ENTITY ORIG NOT FOUND: %s\n", id_orig);
#endif
        return;
    }

    entity_dest = entity_search(id_dest);
    if (entity_dest == NULL) {
#ifdef INFO
        printf("ENTITY DEST NOT FOUND: %s\n", id_dest);
#endif
        return;
    }

    relation = relation_search(id_rel);
    if (relation == NULL) {
        return;
    }

    bot = 0;
    top = relation->valid_entity_relations - 1;
    mid = (bot + top) / 2;
    while (bot <= top) {
        compare = strcmp(entity_orig->id_ent, relation->entity_relations[mid].entity->id_ent);

        if (compare < 0) {
            top = mid - 1;
        } else if (compare > 0) {
            bot = mid + 1;
        } else {
            break;
        }

        mid = (bot + top) / 2;
    }

    entity_relation_id = mid;
    entity_relation = &relation->entity_relations[mid];

    if (!entity_is_equal(entity_relation->entity, entity_orig)) {
        return;
    }

    for (i = 0; i < entity_relation->valid_related_entities; i++) {
        if (entity_is_equal(entity_relation->related_entities[i], entity_dest)) {
            entity_relation->valid_related_entities--;
            entity_relation->related_entities[i] = entity_relation->related_entities[entity_relation->valid_related_entities];
            entity_relation->related_entities[entity_relation->valid_related_entities] = NULL;
            break;
        }
    }

    if (entity_relation->valid_related_entities == 0) {
        entity_relation->entity = NULL;
        relation->valid_entity_relations--;

        for (i = entity_relation_id; i < relation->valid_entity_relations; i++) {
            entity_relation_temp = relation->entity_relations[i];
            relation->entity_relations[i] = relation->entity_relations[i + 1];
            relation->entity_relations[i + 1] = entity_relation_temp;
        }
#ifdef INFO
        printf("Removed %s\n", entity_orig->id_ent);
#endif
    }

#ifdef INFO
    printf("Executed delrel on %s %s %s\n", id_orig, id_dest, id_rel);
#endif

#ifdef DEBUG
    debug_print_relations();
#endif
}

void report() {
    int i, j;
    int max_entity_relations_count, count, is_next;
    relation_t * relation;
    entity_relation_t * entity_relation;

#ifdef INFO
    printf("Executing report\n");
#endif

#ifdef DEBUG
    debug_print_relations();
#endif

    count = 0;
    is_next = 0;
    for (i = 0; i < valid_relations; i++) {
        relation = &relations[i];

        max_entity_relations_count = 0;

        for (j = 0; j < relation->valid_entity_relations; j++) {
            entity_relation = &relation->entity_relations[j];

            if (entity_relation->valid_related_entities > max_entity_relations_count) {
                max_entity_relations_count = entity_relation->valid_related_entities;
            }
        }

        if (max_entity_relations_count > 0) {
            count++;

            if (is_next) {
                fputs(" ", stdout);
            }
            fputs("\"", stdout);
            fputs(relation->id_rel, stdout);
            fputs("\" ", stdout);
            for (j = 0; j < relation->valid_entity_relations; j++) {
                entity_relation = &relation->entity_relations[j];

                if (entity_relation->valid_related_entities == max_entity_relations_count) {
                    fputs("\"", stdout);
                    fputs(entity_relation->entity->id_ent, stdout);
                    fputs("\" ", stdout);
                }
            }

            printf("%d", max_entity_relations_count);
            fputs(";", stdout);
            is_next = 1;
        }
    }

    if (count == 0) {
        fputs("none", stdout);
    }

    fputs("\n", stdout);
}


/* Entities table */

void entities_init() {
    int i;

    entities = malloc(sizeof(entity_t) * ENTITY_DB_SIZE);
    entities_length = ENTITY_DB_SIZE;
    entitiesTable = calloc(ENTITY_DB_SIZE, sizeof(entity_t *));

    for (i = 0; i < entities_length; i++) {
        entities[i].valid = 0;
    }
}

void entities_grow() {
    int last_entities_length, i, j, k;
    unsigned long difference;
    entity_t * entities_old;
    relation_t * relation;
    entity_relation_t * entity_relation;

    entities_old = &entities[0];
    last_entities_length = entities_length;
    entities_length *= 2;
    entities = realloc(entities, sizeof(entity_t) * entities_length);
    entitiesTable = realloc(entitiesTable, sizeof(entity_t *) * entities_length);

    difference = &entities[0] - entities_old;

    for (i = 0; i < valid_relations; i++) {
        relation = &relations[i];

        for (j = 0; j < relation->valid_entity_relations; j++) {
            entity_relation = &relation->entity_relations[j];

            for (k = 0; k < entity_relation->valid_related_entities; k++) {
                entity_relation->related_entities[k] += difference;
            }

            entity_relation->entity += difference;
        }
    }

    /* Clear the hash table and init entities */
    for (i = 0; i < entities_length; i++) {
        if (i >= last_entities_length) {
            entities[i].valid = 0;
        } else if (entities[i].valid) {
            entities[i].hash = entity_hash(entities[i].id_ent);
        }
        entitiesTable[i] = NULL;
    }

    /* Reconstruct the hash table */
    for (i = 0; i < last_entities_length; i++) {
        if (entities[i].valid) {
            entity_add(&entities[i]);
        }
    }
}

int entity_create(char * id_ent, int hash) {
    int i, foundEntityIndex;
    foundEntityIndex = -1;

    for (i = 0; i < entities_length; i++) {
        if (entities[i].valid == 0) {
            foundEntityIndex = i;
            break;
        }
    }

    if (foundEntityIndex == -1) {
        foundEntityIndex = entities_length;
        entities_grow();
    }

    entities[foundEntityIndex].valid = 1;
    entities[foundEntityIndex].hash = hash;
    strcpy(entities[foundEntityIndex].id_ent, id_ent);
    entity_add(&entities[foundEntityIndex]);
    return foundEntityIndex;
}

/* Murmur2 */
int entity_hash(const char * id_ent) {
    const unsigned int magic = 0x5bd1e995;
    const unsigned int rand = 24;
    const unsigned char * data = (const unsigned char *) id_ent;
    unsigned int length;
    unsigned int hash;

    hash = 5563;
    length = strlen(id_ent);

    while (length >= 4) {
        unsigned int key = *(int*)data;

        key *= magic;
        key ^= key >> rand;
        key *= magic;

        hash *= magic;
        hash ^= key;

        data += 4;
        length -= 4;
    }

    switch (length) {
        case 3: hash ^= data[2] << 16;
        case 2: hash ^= data[1] << 8;
        case 1: hash ^= data[0];
        default: hash *= magic;
    }

    hash ^= hash >> 13;
    hash *= magic;
    hash ^= hash >> 15;

    return hash & (entities_length - 1); // entities_length is a power of two
}

entity_t * entity_search(char * id_ent) {
    int hash;
    hash = entity_hash(id_ent);
    return entity_search_with_hash(id_ent, hash);
}

entity_t * entity_search_with_hash(char * id_ent, int hash) {
    int i;
#ifdef COLLISIONS
    int collision;
    collision = 0;
    table_searches++;
#endif

    i = hash;
    while (entitiesTable[i] != NULL) {
        if (entity_is_equal_mixed(entitiesTable[i], id_ent, hash)) {
            return entitiesTable[i];
        }

#ifdef COLLISIONS
        if (collision == 0) {
            printf("COLLISION: %s <-> %s (Hash: %d)\n", entitiesTable[i]->id_ent, id_ent, hash);
            table_collisions++;
            collision = 1;
        }
#endif

        i++;
        i = i & (entities_length - 1); // entities_length is a power of two

        if (i == hash) {
            break;
        }
    }

    return NULL;
}

void entity_add(entity_t * entity) {
    int i, hash;

    hash = entity_hash(entity->id_ent);
    i = hash;
    while (entitiesTable[i] != NULL) {
        if (entity_is_equal(entitiesTable[i], entity)) {
            return;
        }

        i++;
        i = i & (entities_length - 1); // entities_length is a power of two;

        if (i == hash) {
            break;
        }
    }

#ifdef DEBUG_ENTITIES
    printf("ENTITYTABLE: adding %s in %d\n", entity->id_ent, i);
    debug_print_entitiesTable();
#endif

    entitiesTable[i] = entity;

#ifdef DEBUG_ENTITIES
    debug_print_entitiesTable();
    printf("ENTITYTABLE: added %s in %d\n", entity->id_ent, i);
#endif
}

int entity_is_equal(entity_t * entity1, entity_t * entity2) {
    return entity1 == entity2;
}

int entity_is_equal_mixed(entity_t * entity1, char * id_ent2, int hash2) {
    if (entity1 == NULL) {
        return 0;
    }

    if (entity1->hash != hash2) {
        return 0;
    }

    return entity_is_equal_by_id(entity1->id_ent, id_ent2);
}

int entity_is_equal_by_id(char * id_ent1, char * id_ent2) {
    if (strcmp(id_ent1, id_ent2) == 0) {
        return 1;
    }

    return 0;
}

/* End Entities table */


/* Relations table */

void relations_init() {
    int i, j, k;
    entity_relation_t * entity_relation_init;

    relations = malloc(RELATION_DB_SIZE *
            (sizeof(relation_t) + ENTITY_RELATION_SIZE *
            (sizeof(entity_relation_t) + ENTITY_RELATION_RELATED_ENTITIES_SIZE *
            (sizeof(entity_t *))))
            );

    relations_length = RELATION_DB_SIZE;
    valid_relations = 0;

    for (i = 0; i < relations_length; i++) {
        relations[i].valid = 0;
        relations[i].entity_relations_length = ENTITY_RELATION_SIZE;
        relations[i].entity_relations = malloc(ENTITY_RELATION_SIZE *
                (sizeof(entity_relation_t) + ENTITY_RELATION_RELATED_ENTITIES_SIZE *
                (sizeof(entity_t *))));

        for (j = 0; j < relations[i].entity_relations_length; j++) {
            entity_relation_init = &relations[i].entity_relations[j];
            entity_relation_init->entity = NULL;
            entity_relation_init->valid_related_entities = 0;
            entity_relation_init->related_entities_length = ENTITY_RELATION_RELATED_ENTITIES_SIZE;
            entity_relation_init->related_entities = malloc(sizeof(entity_t *) * entity_relation_init->related_entities_length);
            for (k = 0; k < entity_relation_init->related_entities_length; k++) {
                entity_relation_init->related_entities[k] = NULL;
            }
        }
    }
}

void relations_grow() {
    int i, j, k, last_relations_length;
    unsigned long previous_size;
    entity_relation_t * entity_relation_init;

    last_relations_length = relations_length;
    relations_length = last_relations_length * 2;

    previous_size = 0;
    for (i = 0; i < last_relations_length; i++) {
        previous_size += sizeof(relation_t);
        previous_size += relations[i].entity_relations_length * sizeof(entity_relation_t);
        for (j = 0; j < relations[i].entity_relations_length; j++) {
            previous_size += relations[i].entity_relations[j].related_entities_length * sizeof(entity_t *);
        }
    }

    relations = realloc(relations, previous_size + last_relations_length *
            (sizeof(relation_t) + ENTITY_RELATION_SIZE *
            (sizeof(entity_relation_t) + ENTITY_RELATION_RELATED_ENTITIES_SIZE *
            (sizeof(entity_t *))))
    );

    /* Clear the hash table and init entities */
    for (i = 0; i < relations_length; i++) {
        if (i >= last_relations_length) {
            relations[i].valid = 0;
            relations[i].entity_relations_length = ENTITY_RELATION_SIZE;
            relations[i].entity_relations = malloc(ENTITY_RELATION_SIZE *
                    (sizeof(entity_relation_t) + ENTITY_RELATION_RELATED_ENTITIES_SIZE *
                    (sizeof(entity_t *)))
            );

            for (j = 0; j < relations[i].entity_relations_length; j++) {
                entity_relation_init = &relations[i].entity_relations[j];
                entity_relation_init->entity = NULL;
                entity_relation_init->valid_related_entities = 0;
                entity_relation_init->related_entities_length = ENTITY_RELATION_RELATED_ENTITIES_SIZE;
                entity_relation_init->related_entities = malloc(sizeof(entity_t *) * entity_relation_init->related_entities_length);
                for (k = 0; k < entity_relation_init->related_entities_length; k++) {
                    entity_relation_init->related_entities[k] = NULL;
                }
            }
        }
    }
}

relation_t * relation_create(char * id_rel) {
    int i, relation_index;
    relation_t * relation;
    relation_t relation_temp;

    relation_index = -1;

    for (i = 0; i < valid_relations; i++) {
        if (strcmp(id_rel, relations[i].id_rel) < 0) {
            relation_index = i;
            break;
        }
    }

    if (relation_index == -1) {
        relation_index = valid_relations;
    }

    if (valid_relations >= relations_length) {
        relations_grow();
    }

    for (i = valid_relations; i > relation_index; i--) {
        relation_temp = relations[i];
        relations[i] = relations[i - 1];
        relations[i - 1] = relation_temp;
    }

    relation = &relations[relation_index];

    relation->valid = 1;
    strcpy(relation->id_rel, id_rel);
    relation->valid_entity_relations = 0;
    valid_relations++;
    return relation;
}

relation_t * relation_search(char * id_rel) {
    int bot, mid, top, compare;

    if (valid_relations <= 0) {
        return NULL;
    }

    bot = 0;
    top = valid_relations - 1;
    mid = (bot + top) / 2;

    while (bot <= top) {
        compare = strcmp(id_rel, (&relations[mid])->id_rel);

        if (compare < 0) {
            top = mid - 1;
        } else if (compare > 0) {
            bot = mid + 1;
        } else {
            return &relations[mid];
        }

        mid = (bot + top) / 2;
    }

    if (strcmp(id_rel, (&relations[mid])->id_rel) == 0) {
        return &relations[mid];
    }

    return NULL;
}

/* End Relations table */


/* Entity relations */

void entity_relations_grow(relation_t * relation) {
    int i, j, last_entity_relations_length;
    unsigned long previous_size;
    entity_relation_t * entity_relation_init;

    last_entity_relations_length = relation->entity_relations_length;
    previous_size = last_entity_relations_length * sizeof(entity_relation_t);
    for (i = 0; i < relation->entity_relations_length; i++) {
        previous_size += relation->entity_relations[i].related_entities_length * sizeof(entity_t *);
    }

    relation->entity_relations = realloc(relation->entity_relations, previous_size + relation->entity_relations_length *
                                                                     (sizeof(entity_relation_t) + ENTITY_RELATION_RELATED_ENTITIES_SIZE *
                                                                     (sizeof(entity_t *))));
    relation->entity_relations_length = relation->entity_relations_length * 2;

    for (i = last_entity_relations_length; i < relation->entity_relations_length; i++) {
        entity_relation_init = &relation->entity_relations[i];
        entity_relation_init->entity = NULL;
        entity_relation_init->valid_related_entities = 0;
        entity_relation_init->related_entities_length = ENTITY_RELATION_RELATED_ENTITIES_SIZE;
        entity_relation_init->related_entities = malloc(sizeof(entity_t *) * entity_relation_init->related_entities_length);
        for (j = 0; j < entity_relation_init->related_entities_length; j++) {
            entity_relation_init->related_entities[j] = NULL;
        }
    }
}

entity_relation_t * entity_relation_add(relation_t * relation, entity_t * entity) {
    int i, new_entity_relation_index;
    entity_relation_t *entity_relation;
    entity_relation_t *new_entity_relation;
    entity_relation_t entity_relation_temp;

    if (relation->valid_entity_relations >= relation->entity_relations_length) {
        entity_relations_grow(relation);
    }

    for (new_entity_relation_index = 0;
         new_entity_relation_index < relation->valid_entity_relations; new_entity_relation_index++) {
        entity_relation = &relation->entity_relations[new_entity_relation_index];

        if (strcmp(entity->id_ent, entity_relation->entity->id_ent) < 0) {
            break;
        }
    }

    new_entity_relation = &relation->entity_relations[new_entity_relation_index];

    if (new_entity_relation->entity != NULL) {
        for (i = relation->valid_entity_relations; i > new_entity_relation_index; i--) {
            entity_relation_temp = relation->entity_relations[i];
            relation->entity_relations[i] = relation->entity_relations[i - 1];
            relation->entity_relations[i - 1] = entity_relation_temp;
        }
    }

    new_entity_relation->entity = entity;
    relation->valid_entity_relations++;
    return new_entity_relation;
}

entity_relation_t * entity_relation_search(relation_t * relation, entity_t * entity) {
    if (entity == NULL) {
        return NULL;
    }

    return entity_relation_search_by_id(relation, entity->id_ent);
}

entity_relation_t * entity_relation_search_by_id(relation_t * relation, char * id_ent) {
    int bot, mid, top, compare;
    entity_relation_t * entity_relation;

    if (relation->valid_entity_relations <= 0) {
        return NULL;
    }

    bot = 0;
    top = relation->valid_entity_relations - 1;
    mid = (bot + top) / 2;

    while (bot <= top) {
        entity_relation = &relation->entity_relations[mid];
        compare = strcmp(id_ent, entity_relation->entity->id_ent);

        if (compare < 0) {
            top = mid - 1;
        } else if (compare > 0) {
            bot = mid + 1;
        } else {
            return entity_relation;
        }

        mid = (bot + top) / 2;
    }

    entity_relation = &relation->entity_relations[mid];
    if (strcmp(id_ent, entity_relation->entity->id_ent) == 0) {
        return entity_relation;
    }

    return NULL;
}

void entity_relation_add_related(entity_relation_t * entity_relation, entity_t * related) {
    int i;

    for (i = 0; i < entity_relation->valid_related_entities; i++) {
        if (entity_is_equal(entity_relation->related_entities[i], related)) {
            return;
        }
    }

    if (entity_relation->valid_related_entities < entity_relation->related_entities_length) {
        entity_relation->related_entities[entity_relation->valid_related_entities] = related;
        entity_relation->valid_related_entities++;
        return;
    }

    entity_relation->related_entities_length = entity_relation->related_entities_length * 2;
    entity_relation->related_entities = realloc(entity_relation->related_entities, sizeof(entity_t *) * entity_relation->related_entities_length);

    entity_relation->related_entities[i] = related;
    entity_relation->valid_related_entities++;

    for (i = i + 1; i < entity_relation->related_entities_length; i++) {
        entity_relation->related_entities[i] = NULL;
    }
}

/* End Entity relations */


/* Generic functions */

void relations_remove_entity(entity_t * entity) {
    int i, j, k, removed_entity_relation;
    relation_t * relation;
    entity_relation_t * entity_relation;
    entity_relation_t entity_relation_temp;

    for (i = 0; i < valid_relations; i++) {
        relation = &relations[i];

        for (j = 0; j < relation->valid_entity_relations; j++) {
            removed_entity_relation = 0;
            entity_relation = &relation->entity_relations[j];

            if (entity_is_equal(entity_relation->entity, entity)) {
                entity_relation->entity = NULL;
                removed_entity_relation = 1;
                relation->valid_entity_relations--;

                for (k = 0; k < entity_relation->valid_related_entities; k++) {
                    entity_relation->related_entities[k] = NULL;
                }
                entity_relation->valid_related_entities = 0;
            } else {
                for (k = 0; k < entity_relation->valid_related_entities; k++) {
                    if (entity_is_equal(entity_relation->related_entities[k], entity)) {
                        entity_relation->valid_related_entities--;
                        entity_relation->related_entities[k] = entity_relation->related_entities[entity_relation->valid_related_entities];
                        entity_relation->related_entities[entity_relation->valid_related_entities] = NULL;
                    }
                }

                if (entity_relation->valid_related_entities == 0) {
                    entity_relation->entity = NULL;
                    removed_entity_relation = 1;
                    relation->valid_entity_relations--;
                }
            }

            if (removed_entity_relation) {
                for (k = j; k < relation->valid_entity_relations; k++) {
                    entity_relation_temp = relation->entity_relations[k];
                    relation->entity_relations[k] = relation->entity_relations[k + 1];
                    relation->entity_relations[k + 1] = entity_relation_temp;
                }

                j--;
            }
        }
    }
}

/* End Generic functions */


/* Debug functions */

#ifdef DEBUG
void debug_print_entities() {
    int i;

    for (i = 0; i < entities_length; i++) {
        printf("%d -> %s [%d]\n", i, entities[i].id_ent, entities[i].valid);
    }
}


void debug_print_entitiesTable() {
    int i;

    for (i = 0; i < entities_length; i++) {
        if (entitiesTable[i] != NULL) {
            printf("%d -> %s [valid: %d]\n", i, entitiesTable[i]->id_ent, entitiesTable[i]->valid);
        } else {
            printf("%d -> NULL\n", i);
        }
    }
}

void debug_print_relations() {
    int i, j, k;
    entity_relation_t * entity_relation;

    for (i = 0; i < relations_length; i++) {
        printf("%d -> %s [%d]\n", i, relations[i].id_rel, relations[i].valid);
        for (j = 0; j < relations[i].entity_relations_length; j++) {
            entity_relation = &relations[i].entity_relations[j];
            if (entity_relation->entity == NULL) {
                printf("    %d: NULL\n", j);
            } else if (entity_relation->entity->valid == 0) {
                printf("    %d: INVALID\n", j);
            } else {
                printf("    %d: %s\n", j, entity_relation->entity->id_ent);
            }

            for (k = 0; k < entity_relation->related_entities_length; k++) {
                if (entity_relation->related_entities[k] == NULL) {
                    printf("        %d: NULL\n", k);
                } else if (entity_relation->related_entities[k]->valid == 0) {
                    printf("        %d: INVALID\n", k);
                } else {
                    printf("        %d: %s\n", k, entity_relation->related_entities[k]->id_ent);
                }
            }
        }
    }
}
#endif

/* End Debug functions */