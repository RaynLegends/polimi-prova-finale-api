#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ENTITY_DB_SIZE 128
#define ENTITY_DB_INCREMENT 32
#define MAX_INPUT_LINE 128
#define COMMAND_LENGTH 6

#define MAX_ID_ENT_SIZE 32
#define MAX_ID_REL_SIZE 32

#define ERR_UNKNOWN_COMMAND 1
#define ERR_NO_END 2

/* Structs */
typedef struct entity_s {
  int nid;
  char id_ent[MAX_ID_ENT_SIZE];
} entity_t;

typedef struct entity_relation_s {
    entity_t ** entities; /* Array of related entities */
} entity_relation_t;

typedef struct relation_s {
    char id_rel[MAX_ID_REL_SIZE];
    entity_relation_t * entity_relations;
} relation_t;
/* End of Structs */


/* Fields */
entity_t * entities;
int entities_length;
int entity_id_pool[ENTITY_DB_SIZE];
/* End of Fields */


/* Prototypes */
void addent(char * id_ent);
void delent(char * id_ent);
void addrel(char * id_orig, char * id_dest, char * id_rel);
void delrel(char * id_orig, char * id_dest, char * id_rel);
void report();

entity_t * get_entity(char * id_ent);
/* End of Prototypes */


int main(int argc, char * argv[]) {
  int i;
  int scan;
  char character;

  char command[COMMAND_LENGTH];
  char line[MAX_INPUT_LINE];

  char id_ent1[MAX_ID_ENT_SIZE];
  char id_ent2[MAX_ID_ENT_SIZE];
  char id_rel[MAX_ID_REL_SIZE];

  scan = 1;

  entities = malloc(sizeof(entity_t) * ENTITY_DB_SIZE);
  entities_length = ENTITY_DB_SIZE;

  /* Reserve the first entity */
  entities[0].nid = -1;
  entities[0].id_ent[0] = '\0';
  entity_id_pool[0] = -1;

  for (i = 1; i < entities_length; i++) {
    /* Init entities as invalid */
    entities[i].nid = -1;
    entity_id_pool[i] = i;
  }

  while (scan) {
    i = 0;
    while ((character = getchar()) != EOF) {
      if (character == '\n') {
        break;
      }

      if (i < COMMAND_LENGTH) {
        command[i] = character;
      }

      line[i] = character;

      if (i == COMMAND_LENGTH) {
        command[i] = '\0';
      }

      i++;
    }
    command[i] = '\0';
    line[i] = '\0';

    /* printf("%s\n", line); */

    if (strcmp(command, "addent") == 0) {
      sscanf(line, "addent %s", id_ent1);
      addent(id_ent1);
    } else if (strcmp(command, "delent") == 0) {
      sscanf(line, "delent %s", id_ent1);
      delent(id_ent1);
    } else if (strcmp(command, "addrel") == 0) {
      sscanf(line, "addrel %s %s %s", id_ent1, id_ent2, id_rel);
      addrel(id_ent1, id_ent2, id_rel);
    } else if (strcmp(command, "addrel") == 0) {
      sscanf(line, "delrel %s %s %s", id_ent1, id_ent2, id_rel);
      delrel(id_ent1, id_ent2, id_rel);
    } else if (strcmp(command, "report") == 0) {
      report();
    } else if (strcmp(command, "end") == 0) {
      scan = 0;
    } else if (character == EOF) {
      return ERR_NO_END;
    } else {
      return ERR_UNKNOWN_COMMAND;
    }
  }

  return 0;
}

void addent(char * id_ent) {
  //printf("Executing addent on %s\n", id_ent);

  if (get_entity(id_ent)->nid >= 0) {
    printf("Entity found: %d\n", get_entity(id_ent)->nid);
    return;
  }

  int index = entities_length;
  for (int i = 0; i < entities_length; i++) {
    if (entity_id_pool[i] >= 0) {
      index = entity_id_pool[i];
      entity_id_pool[i] = -1;
      break;
    }
  }

  if (index == entities_length) {
    /* Realloc entities array */
    exit(3);
  }

  entities[index].nid = index;
  strcpy(entities[index].id_ent, id_ent);
  printf("Added entity, id %d\n", index);
}

void delent(char * id_ent) {
  entity_t * entity;
  int removed_id;

  //printf("Executing delent on %s\n", id_ent);
  entity = get_entity(id_ent);

  if (get_entity(id_ent)->nid < 0) {
    printf("Entity not found: %s\n", id_ent);
    return;
  }

  removed_id = entity->nid;

  entity_id_pool[removed_id] = removed_id;
  entity->nid = -1;
  entity->id_ent[0] = '\0';
  printf("Removed entity %d\n", removed_id);
}

void addrel(char * id_orig, char * id_dest, char * id_rel) {
  printf("Executing addrel on %s %s %s\n", id_orig, id_dest, id_rel);
}

void delrel(char * id_orig, char * id_dest, char * id_rel) {
  printf("Executing delrel on %s %s %s\n", id_orig, id_dest, id_rel);
}

void report() {
  printf("Executing report\n");
}

entity_t * get_entity(char * id_ent) {
  int i;
  entity_t * entity;

  /* TODO: Implement hash-based search */
  for (i = 0; i < entities_length; i++) {
    entity = &entities[i];
    if (entity->nid >= 0) {
      if (strcmp(entity->id_ent, id_ent) == 0) {
        return entity;
      }
    }
  }

  return &entities[0];
}
