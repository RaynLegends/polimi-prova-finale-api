./build.sh
echo "### VALGRIND ###"
rm callgrind.out.*
valgrind --tool=callgrind ./a.out < test/multiple.in > test/.out
echo "### CALLGRIND ###"
callgrind_annotate callgrind.out.* | grep /home/rayn
rm a.out
rm callgrind.out.*
echo "### DIFF ###"
diff --ignore-trailing-space test/.out test/.out_ok | grep "^>" | wc -l
