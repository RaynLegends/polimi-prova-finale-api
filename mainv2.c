#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*#define DEBUG_ENTITIES
#define DEBUG 1
#define INFO 1*/

#define ENTITY_DB_SIZE 8192
#define ENTITY_DB_INCREMENT 32
#define RELATION_DB_SIZE 4
#define RELATION_DB_INCREMENT 1
#define ENTITY_RELATION_SIZE 16
#define ENTITY_RELATION_INCREMENT 4
#define ENTITY_RELATION_RELATED_ENTITIES_SIZE 8
#define ENTITY_RELATION_RELATED_ENTITIES_INCREMENT 4

#define COMMAND_LENGTH 6

#define MAX_ID_ENT_SIZE 35
#define MAX_ID_REL_SIZE 35

#define ERR_UNKNOWN_COMMAND 1
#define ERR_NO_END 2

/* Structs */
typedef struct entity_s {
    int valid;
    char id_ent[MAX_ID_ENT_SIZE];
} entity_t;

typedef struct entity_relation_s {
    entity_t * entity;
    int related_entities_length;
    entity_t ** related_entities; /* Array of related entities */
} entity_relation_t;

typedef struct relation_s {
    int valid;
    char id_rel[MAX_ID_REL_SIZE];
    int valid_entity_relations;
    int entity_relations_length;
    entity_relation_t * entity_relations;
} relation_t;
/* End of Structs */


/* Fields */
entity_t * entities;
int last_invalid_entity;
int entities_length;

relation_t * relations;
int last_invalid_relation;
int relations_length;

entity_t ** entitiesTable;
relation_t ** relationsTable;
/* End of Fields */


/* Prototypes */
void addent(char * id_ent);
void delent(char * id_ent);
void addrel(char * id_orig, char * id_dest, char * id_rel);
void delrel(char * id_orig, char * id_dest, char * id_rel);
void report();


void entities_init();
void entities_grow();
int entity_create(char * id_ent);
int entity_hash(const char * id_ent);
entity_t * entity_search(char * id_ent);
void entity_add(entity_t * entity);
int entity_is_equal(entity_t * entity1, entity_t * entity2);
int entity_is_equal_by_id(char * id_ent1, char * id_ent2);


void relations_init();
void relations_grow();
relation_t * relation_create(char * id_rel);
int relation_hash(const char * id_rel);
relation_t * relation_search(char * id_rel);
void relation_add(relation_t * relation);
int relation_is_equal_by_id(char * id_rel1, char * id_rel2);
entity_relation_t * entity_relation_add(relation_t * relation, entity_t * entity);
entity_relation_t * entity_relation_search(relation_t * relation, entity_t * entity);
entity_relation_t * entity_relation_search_by_id(relation_t * relation, char * id_ent);
void entity_relation_add_related(entity_relation_t * entity_relation, entity_t * related);


void relations_remove_entity(entity_t * entity);
void sort_relation_ids(char ** relation_ids, int size);
void sort_entity_ids(char ** entity_ids, int size);

#ifdef DEBUG
void debug_print_entities();
void debug_print_entitiesTable();
void debug_print_relations();
#endif
/* End of Prototypes */


int main(int argc, char * argv[]) {
    int i;
    int scan;
    int arg;
    int argpos;
    char character;

    char command[COMMAND_LENGTH + 1];

    char id_ent1[MAX_ID_ENT_SIZE];
    char id_ent2[MAX_ID_ENT_SIZE];
    char id_rel[MAX_ID_REL_SIZE];

    scan = 1;

    entities_init();
    relations_init();

    while (scan) {
        i = 0;
        arg = 0;
        argpos = 0;
        while ((character = getchar()) != EOF) {
            if (character == '\n') {
                if (arg == 0) {
                    id_ent1[argpos] = '\0';
                } else if (arg == 1) {
                    id_ent2[argpos] = '\0';
                } else if (arg == 2) {
                    id_rel[argpos] = '\0';
                }
                break;
            }

            if (i < COMMAND_LENGTH) {
                command[i] = character;
            } else if (i == COMMAND_LENGTH) {
                command[i] = '\0';
            } else if (character == ' ') {
                if (arg == 0) {
                    id_ent1[argpos] = '\0';
                } else if (arg == 1) {
                    id_ent2[argpos] = '\0';
                } else if (arg == 2) {
                    id_rel[argpos] = '\0';
                }
                arg++;
                argpos = 0;
            } else {
                if (arg == 0) {
                    id_ent1[argpos++] = character;
                } else if (arg == 1) {
                    id_ent2[argpos++] = character;
                } else if (arg == 2) {
                    id_rel[argpos++] = character;
                }
            }

            i++;
        }

#ifdef DEBUG
        printf("\n\n\n");
#endif

        if (strcmp(command, "addent") == 0) {
            addent(id_ent1);
        } else if (strcmp(command, "delent") == 0) {
            delent(id_ent1);
        } else if (strcmp(command, "addrel") == 0) {
            addrel(id_ent1, id_ent2, id_rel);
        } else if (strcmp(command, "delrel") == 0) {
            delrel(id_ent1, id_ent2, id_rel);
        } else if (strcmp(command, "report") == 0) {
            report();
        } else if (command[0] == 'e' && command[1] == 'n' && command[2] == 'd') {
            scan = 0;
        } else if (character == EOF) {
            return ERR_NO_END;
        } else {
            return ERR_UNKNOWN_COMMAND;
        }
    }

    return 0;
}

void addent(char * id_ent) {
    entity_t * entity = entity_search(id_ent);

    if (entity != NULL) {
#ifdef INFO
        printf("Entity found: %s - %d\n", entity->id_ent, entity->valid);
#endif
        entity->valid = 1;
        return;
    }

    entity_create(id_ent);

#ifdef INFO
    printf("Added entity %s\n", id_ent);
#endif
}

void delent(char * id_ent) {
    entity_t * entity = entity_search(id_ent);

    if (entity == NULL) {
#ifdef INFO
        printf("Entity not found: %s\n", id_ent);
#endif
        return;
    }

    relations_remove_entity(entity);

    entity->valid = 0;
    entity->id_ent[0] = '\0';

#ifdef INFO
    printf("Removed entity %s\n", id_ent);
#endif
}

void addrel(char * id_orig, char * id_dest, char * id_rel) {
#ifdef INFO
    printf("Executing addrel on %s %s %s\n", id_orig, id_dest, id_rel);
#endif
    entity_t * entity_orig;
    entity_t * entity_dest;
    relation_t * relation;
    entity_relation_t * entity_relation;

    /* Swap orig and dest so that report data is easier to extract */
    char * temp;
    temp = id_orig;
    id_orig = id_dest;
    id_dest = temp;

    entity_orig = entity_search(id_orig);
    if (entity_orig == NULL) {
#ifdef INFO
        printf("ENTITY ORIG NOT FOUND: %s\n", id_orig);
#endif
        return;
    }

    entity_dest = entity_search(id_dest);
    if (entity_dest == NULL) {
#ifdef INFO
        printf("ENTITY DEST NOT FOUND: %s\n", id_dest);
#endif
        return;
    }

    relation = relation_search(id_rel);
    if (relation == NULL) {
        relation = relation_create(id_rel);
        entity_relation = NULL;
    } else {
        entity_relation = entity_relation_search(relation, entity_orig);
    }

    if (entity_relation == NULL) {
        entity_relation = entity_relation_add(relation, entity_orig);
    }

    entity_relation_add_related(entity_relation, entity_dest);

#ifdef INFO
    printf("Executed addrel on %s %s %s\n", id_orig, id_dest, id_rel);
#endif

#ifdef DEBUG
    debug_print_relations();
#endif
}

void delrel(char * id_orig, char * id_dest, char * id_rel) {
#ifdef INFO
    printf("Executing delrel on %s %s %s\n", id_orig, id_dest, id_rel);
#endif
    entity_t * entity_orig;
    entity_t * entity_dest;
    relation_t * relation;
    entity_relation_t * entity_relation;
    int i, count;

    /* Swap orig and dest so that report data is easier to extract */
    char * temp;
    temp = id_orig;
    id_orig = id_dest;
    id_dest = temp;

    entity_orig = entity_search(id_orig);
    if (entity_orig == NULL) {
#ifdef INFO
        printf("ENTITY ORIG NOT FOUND: %s\n", id_orig);
#endif
        return;
    }

    entity_dest = entity_search(id_dest);
    if (entity_dest == NULL) {
#ifdef INFO
        printf("ENTITY DEST NOT FOUND: %s\n", id_dest);
#endif
        return;
    }

    relation = relation_search(id_rel);
    if (relation == NULL) {
        return;
    }

    entity_relation = entity_relation_search(relation, entity_orig);

    if (entity_relation == NULL) {
        return;
    }

    count = 0;
    for (i = 0; i < entity_relation->related_entities_length; i++) {
        if (entity_relation->related_entities[i] == NULL) {
            continue;
        }

        if (entity_is_equal(entity_relation->related_entities[i], entity_dest)) {
            entity_relation->related_entities[i] = NULL;
        } else {
            count++;
        }
    }

    if (count == 0) {
        entity_relation->entity = NULL;
        relation->valid_entity_relations--;
#ifdef INFO
        printf("Removed %s", entity_orig->id_ent);
#endif
    }

#ifdef INFO
    printf("Executed delrel on %s %s %s\n", id_orig, id_dest, id_rel);
#endif

#ifdef DEBUG
    debug_print_relations();
#endif
}

void report() {
    int i, j, k, kk;
    int relations_count, entity_relations_count, max_entity_relations_count, count, count_relation, is_next;
    relation_t * relation;
    entity_relation_t * entity_relation;
    entity_t ** max_entities;
    char ** relation_ids;
    char ** entity_ids;

#ifdef INFO
    printf("Executing report\n");
#endif

    relations_count = 0;
    for (i = 0; i < relations_length; i++) {
        if (relations[i].valid) {
            relations_count++;
        }
    }
    relation_ids = malloc(sizeof(char *) * relations_count);
    j = 0;
    for (i = 0; i < relations_length; i++) {
        if (relations[i].valid) {
            relation_ids[j] = malloc(sizeof(char) * MAX_ID_REL_SIZE);
            strcpy(relation_ids[j++], relations[i].id_rel);
        }
    }

    sort_relation_ids(relation_ids, relations_count);

    count = 0;
    is_next = 0;
    for (i = 0; i < relations_count; i++) {
        relation = relation_search(relation_ids[i]);
        count_relation = 0;
        max_entity_relations_count = 0;
        max_entities = malloc(sizeof(entity_t *) * relation->entity_relations_length);

        // TODO: Sort by name DESC
        for (j = 0; j < relation->entity_relations_length; j++) {
            entity_relation = &relation->entity_relations[j];
            entity_relations_count = 0;
            max_entities[j] = NULL;

            if (entity_relation->entity == NULL) {
                continue;
            }

            for (k = 0; k < entity_relation->related_entities_length; k++) {
                if (entity_relation->related_entities[k] != NULL) {
                    entity_relations_count++;
                }
            }

            if (entity_relations_count > max_entity_relations_count) {
                max_entity_relations_count = entity_relations_count;
                for (kk = 0; kk < j; kk++) {
                    max_entities[kk] = NULL;
                }
                max_entities[j] = entity_relation->entity;
                count_relation = 1;
            } else if (entity_relations_count == max_entity_relations_count && max_entity_relations_count > 0) {
                max_entities[j] = entity_relation->entity;
                count_relation++;
            }
        }

        if (count_relation > 0) {
            count++;

            // TODO: Sort by name ASC
            entity_ids = malloc(sizeof(char *) * count_relation);
            k = 0;

            if (is_next) {
                printf(" ");
            }
            printf("%s ", relation->id_rel);
            for (j = 0; j < relation->entity_relations_length; j++) {
                if (max_entities[j] != NULL) {
                    entity_ids[k] = malloc(sizeof(char) * MAX_ID_ENT_SIZE);
                    strcpy(entity_ids[k++], max_entities[j]->id_ent);
                }
            }

            sort_entity_ids(entity_ids, count_relation);
            for (j = 0; j < count_relation; j++) {
                printf("%s ", entity_ids[j]);
            }

            printf("%d;", max_entity_relations_count);
            is_next = 1;

            for (j = 0; j < count_relation; j++) {
                free(entity_ids[j]);
            }

            free(entity_ids);
        }

        free(max_entities);
    }

    if (count == 0) {
        printf("none");
    }

    printf("\n");

    for (i = 0; i < relations_count; i++) {
        free(relation_ids[i]);
    }

    free(relation_ids);
}

/* Entities table */

void entities_init() {
    int i;

    entities = malloc(sizeof(entity_t) * ENTITY_DB_SIZE);
    last_invalid_entity = 0;
    entities_length = ENTITY_DB_SIZE;

    entitiesTable = malloc(sizeof(entity_t *) * ENTITY_DB_SIZE);

    for (i = 0; i < entities_length; i++) {
        entities[i].valid = 0;
        entitiesTable[i] = NULL;
    }
}

void entities_grow() {
    int last_entities_length, i;

    last_entities_length = entities_length;
    entities_length = entities_length + ENTITY_DB_INCREMENT;
    entities = realloc(entities, sizeof(entity_t) * entities_length);
    entitiesTable = realloc(entitiesTable, sizeof(entity_t *) * entities_length);
    // TODO: Move pointers inside relations to follow the new location in memory

    /* Clear the hash table and init entities */
    for (i = 0; i < entities_length; i++) {
        if (i >= last_entities_length) {
            entities[i].valid = 0;
        }
        entitiesTable[i] = NULL;
    }

    /* Reconstruct the hash table */
    for (i = 0; i < last_entities_length; i++) {
        if (entities[i].valid) {
            entity_add(&entities[i]);
        }
    }
}

int entity_create(char * id_ent) {
    int i, checked, foundEntityIndex;
    checked = 0;
    foundEntityIndex = -1;

    for (i = last_invalid_entity; i < entities_length; i = i + 1 % entities_length) {
        if (entities[i].valid == 0) {
            foundEntityIndex = i;
            break;
        }

        checked++;

        if (checked >= entities_length) {
            break;
        }
    }

    if (foundEntityIndex == -1) {
        foundEntityIndex = entities_length;
        entities_grow();
    }

    entities[foundEntityIndex].valid = 1;
    strcpy(entities[foundEntityIndex].id_ent, id_ent);
    entity_add(&entities[foundEntityIndex]);
    return foundEntityIndex;
}

int entity_hash(const char * id_ent) {
    int i, code;

    i = 0;
    code = 0;
    while (id_ent[i] != '\0') {
        code += id_ent[i] - '\0';
        i++;
    }

    return code % entities_length;
}

entity_t * entity_search(char * id_ent) {
    int hash = entity_hash(id_ent);
    int i = hash;

    while (entitiesTable[i] != NULL) {
        if (entity_is_equal_by_id(entitiesTable[i]->id_ent, id_ent)) {
            return entitiesTable[i];
        }

        i++;
        i = i % entities_length;

        if (i == hash) {
            break;
        }
    }

    return NULL;
}

void entity_add(entity_t * entity) {
    int hash = entity_hash(entity->id_ent);
    int i = hash;

    while (entitiesTable[i] != NULL) {
        if (entity_is_equal_by_id(entitiesTable[i]->id_ent, entity->id_ent)) {
            return;
        }

        i++;
        i = i % entities_length;

        if (i == hash) {
            break;
        }
    }

#ifdef DEBUG_ENTITIES
    printf("ENTITYTABLE: adding %s in %d\n", entity->id_ent, i);
    debug_print_entitiesTable();
#endif

    entitiesTable[i] = entity;

#ifdef DEBUG_ENTITIES
    debug_print_entitiesTable();
    printf("ENTITYTABLE: added %s in %d\n", entity->id_ent, i);
#endif
}

int entity_is_equal(entity_t * entity1, entity_t * entity2) {
    if (entity1 == NULL || entity2 == NULL) {
        return 0;
    }

    return entity_is_equal_by_id(entity1->id_ent, entity2->id_ent);
}

int entity_is_equal_by_id(char * id_ent1, char * id_ent2) {
    if (strcmp(id_ent1, id_ent2) == 0) {
        return 1;
    }

    return 0;
}

/* End Entities table */


/* Relations table */

void relations_init() {
    int i, j, k;
    entity_relation_t * entity_relation_init;

    relations = malloc(RELATION_DB_SIZE *
            (sizeof(relation_t) + ENTITY_RELATION_SIZE *
            (sizeof(entity_relation_t) + ENTITY_RELATION_RELATED_ENTITIES_SIZE *
            (sizeof(entity_t *))))
            );

    last_invalid_relation = 0;
    relations_length = RELATION_DB_SIZE;

    relationsTable = malloc(sizeof(relation_t *) * RELATION_DB_SIZE);

    for (i = 0; i < relations_length; i++) {
        relations[i].valid = 0;
        relations[i].entity_relations_length = ENTITY_RELATION_SIZE;
        relations[i].entity_relations = malloc(ENTITY_RELATION_SIZE *
                (sizeof(entity_relation_t) + ENTITY_RELATION_RELATED_ENTITIES_SIZE *
                (sizeof(entity_t *))));

        for (j = 0; j < relations[i].entity_relations_length; j++) {
            entity_relation_init = &relations[i].entity_relations[j];
            entity_relation_init->entity = NULL;
            entity_relation_init->related_entities_length = ENTITY_RELATION_RELATED_ENTITIES_SIZE;
            entity_relation_init->related_entities = malloc(sizeof(entity_t *) * entity_relation_init->related_entities_length);
            for (k = 0; k < entity_relation_init->related_entities_length; k++) {
                entity_relation_init->related_entities[k] = NULL;
            }
        }

        relationsTable[i] = NULL;
    }
}

void relations_grow() {
    int i, j, k, last_relations_length;
    unsigned long previous_size;
    entity_relation_t * entity_relation_init;

    last_relations_length = relations_length;
    relations_length = relations_length + RELATION_DB_INCREMENT;

    previous_size = 0;
    for (i = 0; i < last_relations_length; i++) {
        previous_size += sizeof(relation_t);
        previous_size += relations[i].entity_relations_length * sizeof(entity_relation_t);
        for (j = 0; j < relations[i].entity_relations_length; j++) {
            previous_size += relations[i].entity_relations[j].related_entities_length * sizeof(entity_t *);
        }
    }

    relations = realloc(relations, previous_size + RELATION_DB_INCREMENT *
            (sizeof(relation_t) + ENTITY_RELATION_SIZE *
            (sizeof(entity_relation_t) + ENTITY_RELATION_RELATED_ENTITIES_SIZE *
            (sizeof(entity_t *))))
    );
    relationsTable = realloc(relationsTable, sizeof(relation_t *) * relations_length);

    /* Clear the hash table and init entities */
    for (i = 0; i < relations_length; i++) {
        if (i >= last_relations_length) {
            relations[i].valid = 0;
            relations[i].entity_relations_length = ENTITY_RELATION_SIZE;
            relations[i].entity_relations = malloc(ENTITY_RELATION_SIZE *
                    (sizeof(entity_relation_t) + ENTITY_RELATION_RELATED_ENTITIES_SIZE *
                    (sizeof(entity_t *)))
            );

            for (j = 0; j < relations[i].entity_relations_length; j++) {
                entity_relation_init = &relations[i].entity_relations[j];
                entity_relation_init->entity = NULL;
                entity_relation_init->related_entities_length = ENTITY_RELATION_RELATED_ENTITIES_SIZE;
                entity_relation_init->related_entities = malloc(sizeof(entity_t *) * entity_relation_init->related_entities_length);
                for (k = 0; k < entity_relation_init->related_entities_length; k++) {
                    entity_relation_init->related_entities[k] = NULL;
                }
            }
        }
        relationsTable[i] = NULL;
    }

    /* Reconstruct the hash table */
    for (i = 0; i < last_relations_length; i++) {
        if (relations[i].valid) {
            relation_add(&relations[i]);
        }
    }
}

relation_t * relation_create(char * id_rel) {
    int i, checked, foundRelationIndex;
    checked = 0;
    foundRelationIndex = -1;

    for (i = last_invalid_relation; i < relations_length; i = i + 1 % relations_length) {
        if (relations[i].valid == 0) {
            foundRelationIndex = i;
            break;
        }

        checked++;

        if (checked >= relations_length) {
            break;
        }
    }

    if (foundRelationIndex == -1) {
        foundRelationIndex = relations_length;
        relations_grow();
    }

    relations[foundRelationIndex].valid = 1;
    strcpy(relations[foundRelationIndex].id_rel, id_rel);
    relations[foundRelationIndex].valid_entity_relations = 0;
    relation_add(&relations[foundRelationIndex]);
    return &relations[foundRelationIndex];
}

int relation_hash(const char * id_rel) {
    int i, code;

    i = 0;
    code = 0;
    while (id_rel[i] != '\0') {
        code += id_rel[i] - '\0';
        i++;
    }

    return code % relations_length;
}

relation_t * relation_search(char * id_rel) {
    int hash = relation_hash(id_rel);
    int i = hash;

    while (relationsTable[i] != NULL) {
        if (relation_is_equal_by_id(relationsTable[i]->id_rel, id_rel)) {
            return relationsTable[i];
        }

        i++;
        i = i % relations_length;

        if (i == hash) {
            break;
        }
    }

    return NULL;
}

void relation_add(relation_t * relation) {
    int hash = relation_hash(relation->id_rel);
    int i = hash;

    while (relationsTable[i] != NULL) {
        if (relation_is_equal_by_id(relationsTable[i]->id_rel, relation->id_rel)) {
            return;
        }

        i++;
        i = i % relations_length;

        if (i == hash) {
            break;
        }
    }

    relationsTable[i] = relation;
}

int relation_is_equal_by_id(char * id_rel1, char * id_rel2) {
    if (strcmp(id_rel1, id_rel2) == 0) {
        return 1;
    }

    return 0;
}

/* End Relations table */


/* Entity relations */

entity_relation_t * entity_relation_add(relation_t * relation, entity_t * entity) {
    int i, k, last_entity_relations_length;
    unsigned long previous_size;
    entity_relation_t * entity_relation;
    entity_relation_t * new_entity_relation;
    entity_relation_t * entity_relation_init;

    new_entity_relation = NULL;
    previous_size = 0;
    for (i = 0; i < relation->entity_relations_length; i++) {
        entity_relation = &relation->entity_relations[i];

        if (entity_relation->entity == NULL) {
            new_entity_relation = entity_relation;
        }

        previous_size += relation->entity_relations[i].related_entities_length * sizeof(entity_t *);
    }

    if (new_entity_relation != NULL) {
        new_entity_relation->entity = entity;
        relation->valid_entity_relations++;
        return new_entity_relation;
    }

    last_entity_relations_length = relation->entity_relations_length;
    relation->entity_relations_length = relation->entity_relations_length + ENTITY_RELATION_INCREMENT;
    previous_size += last_entity_relations_length * sizeof(entity_relation_t);

    relation->entity_relations = realloc(relation->entity_relations, previous_size + ENTITY_RELATION_INCREMENT *
            (sizeof(entity_relation_t) + ENTITY_RELATION_RELATED_ENTITIES_SIZE *
            (sizeof(entity_t *))));

    entity_relation = &relation->entity_relations[i];

    for (; i < relation->entity_relations_length; i++) {
        entity_relation_init = &relation->entity_relations[i];
        entity_relation_init->entity = NULL;
        entity_relation_init->related_entities_length = ENTITY_RELATION_RELATED_ENTITIES_SIZE;
        entity_relation_init->related_entities = malloc(sizeof(entity_t *) * entity_relation_init->related_entities_length);
        for (k = 0; k < entity_relation_init->related_entities_length; k++) {
            entity_relation_init->related_entities[k] = NULL;
        }
    }

    relation->entity_relations[last_entity_relations_length].entity = entity;

    relation->valid_entity_relations++;
    return entity_relation;
}

entity_relation_t * entity_relation_search(relation_t * relation, entity_t * entity) {
    if (entity == NULL) {
        return NULL;
    }

    return entity_relation_search_by_id(relation, entity->id_ent);
}

entity_relation_t * entity_relation_search_by_id(relation_t * relation, char * id_ent) {
    int i;
    entity_relation_t * entity_relation;

    for (i = 0; i < relation->entity_relations_length; i++) {
        entity_relation = &relation->entity_relations[i];

        if (entity_relation->entity == NULL) {
            continue;
        }

        if (entity_is_equal_by_id(entity_relation->entity->id_ent, id_ent)) {
            return entity_relation;
        }
    }

    return NULL;
}

void entity_relation_add_related(entity_relation_t * entity_relation, entity_t * related) {
    int i, null_index;

    null_index = -1;
    for (i = 0; i < entity_relation->related_entities_length; i++) {
        if (null_index == -1 && entity_relation->related_entities[i] == NULL) {
            null_index = i;
        }

        if (entity_is_equal(entity_relation->related_entities[i], related)) {
            return;
        }
    }

    if (null_index > -1) {
        entity_relation->related_entities[null_index] = related;
        return;
    }

    entity_relation->related_entities_length = entity_relation->related_entities_length + ENTITY_RELATION_RELATED_ENTITIES_INCREMENT;
    entity_relation->related_entities = realloc(entity_relation->related_entities, sizeof(entity_t *) * entity_relation->related_entities_length);

    entity_relation->related_entities[i] = related;

    for (i = i + 1; i < entity_relation->related_entities_length; i++) {
        entity_relation->related_entities[i] = NULL;
    }
}

/* End Entity relations */


/* Generic functions */

void relations_remove_entity(entity_t * entity) {
    int i, j, k;
    relation_t * relation;
    entity_relation_t * entity_relation;

    for (i = 0; i < relations_length; i++) {
        relation = &relations[i];

        for (j = 0; j < relation->entity_relations_length; j++) {
            entity_relation = &relation->entity_relations[j];

            if (entity_relation->entity == NULL) {
                continue;
            }

            if (entity_is_equal(entity_relation->entity, entity)) {
                entity_relation->entity = NULL;

                for (k = 0; k < entity_relation->related_entities_length; k++) {
                    entity_relation->related_entities[k] = NULL;
                }
            } else {
                for (k = 0; k < entity_relation->related_entities_length; k++) {
                    if (entity_is_equal(entity_relation->related_entities[k], entity)) {
                        entity_relation->related_entities[k] = NULL;
                    }
                }
            }
        }
    }
}

/* N^2 sort, ascending */
void sort_relation_ids(char ** relation_ids, int size) {
    int i, j;
    char id_rel_temp[MAX_ID_REL_SIZE];

    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            if (strcmp(relation_ids[i], relation_ids[j]) < 0) {
                strcpy(id_rel_temp, relation_ids[i]);
                strcpy(relation_ids[i], relation_ids[j]);
                strcpy(relation_ids[j], id_rel_temp);
            }
        }
    }
}

/* N^2 sort, descending */
void sort_entity_ids(char ** entity_ids, int size) {
    int i, j;
    char id_ent_temp[MAX_ID_ENT_SIZE];

    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            if (strcmp(entity_ids[i], entity_ids[j]) < 0) {
                strcpy(id_ent_temp, entity_ids[i]);
                strcpy(entity_ids[i], entity_ids[j]);
                strcpy(entity_ids[j], id_ent_temp);
            }
        }
    }
}

/* End Generic functions */


/* Debug functions */

#ifdef DEBUG
void debug_print_entities() {
    int i;

    for (i = 0; i < entities_length; i++) {
        printf("%d -> %s [%d]\n", i, entities[i].id_ent, entities[i].valid);
    }
}


void debug_print_entitiesTable() {
    int i;

    for (i = 0; i < entities_length; i++) {
        if (entitiesTable[i] != NULL) {
            printf("%d -> %s [valid: %d]\n", i, entitiesTable[i]->id_ent, entitiesTable[i]->valid);
        } else {
            printf("%d -> NULL\n", i);
        }
    }
}

void debug_print_relations() {
    int i, j, k;
    entity_relation_t * entity_relation;

    for (i = 0; i < relations_length; i++) {
        printf("%d -> %s [%d]\n", i, relations[i].id_rel, relations[i].valid);
        for (j = 0; j < relations[i].entity_relations_length; j++) {
            entity_relation = &relations[i].entity_relations[j];
            if (entity_relation->entity == NULL) {
                printf("    %d: NULL\n", j);
            } else if (entity_relation->entity->valid == 0) {
                printf("    %d: INVALID\n", j);
            } else {
                printf("    %d: %s\n", j, entity_relation->entity->id_ent);
            }

            for (k = 0; k < entity_relation->related_entities_length; k++) {
                if (entity_relation->related_entities[k] == NULL) {
                    printf("        %d: NULL\n", k);
                } else if (entity_relation->related_entities[k]->valid == 0) {
                    printf("        %d: INVALID\n", k);
                } else {
                    printf("        %d: %s\n", k, entity_relation->related_entities[k]->id_ent);
                }
            }
        }
    }
}
#endif

/* End Debug functions */