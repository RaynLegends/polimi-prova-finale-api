./build.sh
echo "### VALGRIND ###"
rm callgrind.out.*
valgrind --tool=callgrind ./a.out < test/batch1.2.in > test/.out
echo "### CALLGRIND ###"
callgrind_annotate --inclusive=no callgrind.out.* | grep /home/rayn
rm a.out
rm callgrind.out.*
echo "### DIFF ###"
diff --ignore-trailing-space test/.out test/batch1.2.py.out | grep "^>" | wc -l
